Thus far we discovered the Image Reference Tool in Krita[1],
Krita is a free and powerful Digital Art program.
If you prearrange your entire scene in a singe image,
in a Photo Editing program like GIMP[2] or just take a solid photo.
And then stretch the reference image over your entire canvas in Krita,
your color picker will only ever pick colors from your reference image.
Never from what is on your canvas,
but always from your per-arranged scene.
When you are painting, you can just hold down CONTROL key,
and that will temporary switch your brush into a color picker.

The only problem is, you need a Pen and Tablet[3],
and they may cost between $40 and $60 dollars, they don't all cost a $1,000 dollars[4].
You mouse does not have a pressure sensor,
and that is exact what the tip of the pen is.
In Krita this allows you to lay down layers of semi-transparent color,
or use the pressure to make your virtual brush bigger.
A mouse is a very negative thing for starting Digital Art, it is hard to believe,
but it very dangerous, as it has the power to push a person away from art.
Mouse,
Not Even Once.

Art comes from your Heart,
it is like you are a care bear, and the power you shoot is Digital Beauty.
Art loves you so much, that, on the first day,
you sit down with a pen and tablet, and Krita and a reference image over you canvas.
You will create,
a hyper realistic work of art.
You will not be able to stop creating these works,
ever.
And with each one, you learn more and more,
about becoming a more and more powerful artist.

Which brings us to the great challenge,
and my goodness it is the most beauuuuutiful adventure.
Because once you have tasted Hyperrealism, and feel safe and powerful,
you will have to ask one of the most beautiful questions in art:
“What is the minimum number of brushstrokes,
that will capture all the beauty this work deserves?”
In other words, now that you can make all the brush strokes and create perfection,
you ask, how can I create beauty and perfection with the smallest number of brushstrokes.

You may recognize this question,
in all the famous and unique works of art.
The Starry Nigh by Vincent van Gogh[5],
which captures the feeling of genius and individuality, Bukowski's isolation[6], sacredness and comprehension,.
And his self portrait where the detail of the brushwork[7],
is proportional to the importance of the feature.
His jacket holds thick strokes,
where his eyes have such tiny ones that they look perfectly realistic.
It is very important to note, that the brushstrokes are not blended[8],
they are raw, as if Vincent van Gogh was only making a color map of a yet to be created realistic painting.
Claude Monet[9] has a very interesting approach that involves a kind of blur[10],
the areas that are unimportant are left in the oil equivalent of low resolution or blur.

In your research you are bound to discover, digital artists that leave large thick lines[11],
just like Vincent’s leaving his most pronounced brushstrokes.
I hope it calls to you, to know,
that nothing is ever really lost, once you are creating.
Once you had your fill of Hyperrealism,
you will begin to discover, so much more[12].
It is not Esperanto, Mathematics, Physics, Astrophysics, Biology or Philosophy, it is Art,
Art - is the universal language.
Begin with Hyperrealism so that you may truly know the blessing of art,
and slowly start your life long journey towards minimalism, and maximum expression.
Speak out, the world truly deeply wants to know
what you have to say.
 
References
[1]: https://www.youtube.com/watch?v=0uCH2z_zLmc
[2]: https://www.youtube.com/results?search_query=GIMP+Tutorial
[3]: https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Delectronics&field-keywords=++Drawing+Tablet
[4]: https://www.youtube.com/watch?v=rKzUtWvElr4
[5]: https://www.youtube.com/watch?v=6PndwgJuF3g
[6]: https://www.youtube.com/watch?v=k6_QUhUPrF4
[7]: https://www.youtube.com/watch?v=mgxHvV9-liA
[8]: https://www.youtube.com/watch?v=WD7aoa256_0
[9]: https://www.youtube.com/watch?v=4Gk85DFluoE
[10]: https://www.youtube.com/watch?v=M1xvAhriXl0
[11]: https://www.youtube.com/watch?v=Nhqtm_NPSE0
[12]: https://www.youtube.com/c/SaraTepes/videos