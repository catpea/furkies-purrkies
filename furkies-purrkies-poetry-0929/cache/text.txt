I’ve been making web pages,
for ages.
I have learned so much,
just by creating buttons and such.
I just started a looking at code,
and I learned a boat load.

Learning was never too bad,
and I even just used notepad.
At first I learned HTML and ASP,
but then Perl worked hard on the server for me.
It was a beautiful adventure in code
as I jumped to PHP, ActionScript, Java, and now node.
It was all so fascinating,
that I never stopped creating.

If you make programming your aim,
know that it is like a fun game game.
But here you are playing the computer,
and your keyboard is your six shooter.
If you start by learning JavaScript,
you will become well equipped.
Do not delay,
give programming a chance today.
Find a JavaScript tutorial video[1],
and give it a go.
 
References
[1]: https://www.youtube.com/results?search_query=JavaScript+node+tutorial