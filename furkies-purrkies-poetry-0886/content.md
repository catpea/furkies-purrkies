When it comes to a Bush Baby,
the answer is __never__ maybe.

They are as audacious,
as their world is spacious.

And they are really smart,
as they encourage us to art.

---

They know you will try to paint one,
and become an artist before you are done.

And that in turn,
teaches you how to really learn.

---

Real education,
is important to every nation.

And if your schools are gone,
art teaches you to learn on your own.

---

Bush babies are what the world needs,
because they really help the kids.

---

School teaches we are dumb,
and need to get an income.

But with art,
we have proof that we are smart.

And that we are meant to grow,
not just put up a show.

Those who grow to no end,
never have to pretend.

---

The future ahead will grow brighter,
and all our worries will become lighter.

With real education as the norm,
will avoid every storm.

The truly educated will finally fix,
all the world’s politics.

Poverty will end too,
the world will be more authentic and true.

It will move to converge on wisdom and peace,
and all the fighting will cease.

---

This communication was paid for by the Bush Baby Society of America and the Bush Family. I am part Bush Baby by Evolution and President of My Office Of The Land,
and I approve this message on behalf of all bush babies. The top contributors of Bush Baby For President 2024 are [He-Man][0] and the [Gopher from Caddyshack][1].

[0]: https://www.youtube.com/watch?v=7yeA7a0uS3A&t=4s
[1]: https://www.youtube.com/watch?v=WaSUyYSQie8
